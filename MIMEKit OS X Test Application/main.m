//
//  main.m
//  MIMEKit OS X Test Application
//
//  Created by luca on 27/10/13.
//  Copyright (c) 2013 Portable Knowledge, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
